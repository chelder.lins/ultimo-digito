package br.com.pongelupe.dto;

import javax.validation.constraints.NotBlank;

import br.com.pongelupe.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {

	private String userId;

	@NotBlank
	private String name;

	@NotBlank
	// @Email
	private String email;

	public UserDTO(User user) {
		this.userId = user.getUserId();
		this.name = user.getName();
		this.email = user.getEmail();
	}

	public User toEntity() {
		var user = new User();
		user.setUserId(userId);
		user.setName(name);
		user.setEmail(email);

		return user;
	}

}
