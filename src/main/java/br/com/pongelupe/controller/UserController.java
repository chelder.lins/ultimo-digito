package br.com.pongelupe.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.pongelupe.dto.CalculationDTO;
import br.com.pongelupe.dto.PublicKeyDTO;
import br.com.pongelupe.dto.UserDTO;
import br.com.pongelupe.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<UserDTO> createUser(@RequestBody @Valid UserDTO user) {
		return userService.createUser(user)
				.map(userDTO -> ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().path("/{userId}")
							.buildAndExpand(userDTO.getUserId()).toUri())
						.body(userDTO))
				.orElseGet(() -> ResponseEntity.badRequest().build());
	}
	
	@GetMapping(path = "/{userId}")
	public ResponseEntity<UserDTO> readUser(@PathVariable String userId) {
		return userService.readUser(userId)
				.map(ResponseEntity::ok)
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@PutMapping(path = "/{userId}")
	public ResponseEntity<UserDTO> updateUser(@PathVariable String userId, @RequestBody UserDTO user) {
		return userService.updateUser(userId, user)
				.map(ResponseEntity::ok)
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@DeleteMapping(path = "/{userId}")
	public ResponseEntity<?> deleteUser(@PathVariable String userId) {
		return userService.deleteUser(userId)
				.map(userIdDeleted -> ResponseEntity.noContent().build())
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@GetMapping(path = "/{userId}/calculations")
	public ResponseEntity<List<CalculationDTO>> readDigitoUnicoByUser(@PathVariable String userId) {
		return ResponseEntity.ok(userService.readCalculationsByUser(userId));
	}

	@PostMapping(path = "/{userId}/publicKey")
	public ResponseEntity<?> addPublicKeyUser(@PathVariable String userId, @RequestBody PublicKeyDTO publicKey) {
		userService.addPublicKeyToUser(userId, publicKey.getPublicKey());
		return ResponseEntity.ok().build();
	}
}
