package br.com.pongelupe.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.pongelupe.dto.CalculationDTO;
import br.com.pongelupe.service.CalculationService;

@RestController
@RequestMapping("/calculations")
public class CalculationController {

	@Autowired
	private CalculationService calculationService;

	@PostMapping
	public ResponseEntity<CalculationDTO> calculate(@RequestBody @Valid CalculationDTO calculation) {
		CalculationDTO digitoUnico = calculationService.digitoUnico(calculation);
		
		if (digitoUnico.getUserId() != null) {
			return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{userId}")
					.buildAndExpand(calculation.getUserId()).toUri())
				.body(digitoUnico);
		} else {			
			return ResponseEntity.ok(digitoUnico);
		}
	}

}
