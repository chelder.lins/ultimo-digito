package br.com.pongelupe.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This represents a User entity
 * 
 * @author pongelupe
 *
 */
@Entity
@Data
@NoArgsConstructor
public class User {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "user_id", nullable = false, updatable = false)
	private String userId;

	@Lob
	private String name;

	@Lob
	private String email;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Calculation> calculations;

	@Lob
	private String publicKey;

	public User(String userId) {
		this.userId = userId;
	}

}
