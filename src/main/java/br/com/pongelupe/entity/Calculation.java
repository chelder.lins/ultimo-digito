package br.com.pongelupe.entity;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * This represents a Calculation entity
 * 
 * @author pongelupe
 *
 */
@Entity
@Data
@NoArgsConstructor
public class Calculation {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "calculation_id", nullable = false, updatable = false)
	private String calculationId;

	/**
	 * This field represents a {@link java.lang.Integer} ranging 1 to 10^1000000
	 */
	@Lob
	private String n;

	/**
	 * This field represents a {@link java.lang.Integer} ranging 1 to 10^5
	 */
	@Column(length = 100000)
	private Integer k;

	private Integer result;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", updatable = false, insertable = true, nullable = false)
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)
	private String userId;
	
	public void setUser(User user) {
		this.user = user;
		Optional.ofNullable(user)
			.ifPresent(u -> setUserId(u.getUserId()));
	}
}
