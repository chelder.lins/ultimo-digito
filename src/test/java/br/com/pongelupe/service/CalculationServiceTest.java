package br.com.pongelupe.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import br.com.pongelupe.dto.CalculationDTO;
import br.com.pongelupe.entity.User;
import br.com.pongelupe.repository.CalculationRepository;
import br.com.pongelupe.repository.UserRepository;

public class CalculationServiceTest {

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@InjectMocks
	private CalculationService calculationService;

	@Mock
	private CalculationRepository calculationRepository;
	
	@Mock
	private UserRepository userRepository;

	@Test
	public void shouldCalculateLastDigito() {

		assertEquals(2, calculationService.digitoUnico("9875", 1).intValue());
		assertEquals(8, calculationService.digitoUnico("9875", 4).intValue());
		assertEquals(8, calculationService.digitoUnico("9875", 4).intValue());
		assertEquals(9, calculationService.digitoUnico("1998", 10).intValue());
	}

	@Test
	public void shouldCalculateLastDigitoWithUser() {
		// given
		var calculationDTO = new CalculationDTO();
		calculationDTO.setN("9875");
		calculationDTO.setK(4);
		var userIdMocked = "SOME-ID";
		calculationDTO.setUserId(userIdMocked);
		when(userRepository.existsById(userIdMocked)).thenReturn(true);
		when(userRepository.getOne(userIdMocked)).thenReturn(new User(userIdMocked));
		var savedCalculationMock = calculationDTO.toEntity();
		savedCalculationMock.setResult(8);
		when(calculationRepository.save(savedCalculationMock)).thenReturn(savedCalculationMock);

		// when
		var realCalculation = calculationService.digitoUnico(calculationDTO);

		// Assert
		assertEquals(8, realCalculation.getResult().intValue());
		assertEquals(userIdMocked, realCalculation.getUserId());
	}
	
	@Test
	public void shouldCalculateLastDigitoWithoutUser() {
		// given
		var calculationDTO = new CalculationDTO();
		calculationDTO.setN("9875");
		calculationDTO.setK(4);
		var savedCalculationMock = calculationDTO.toEntity();
		savedCalculationMock.setResult(8);
		when(calculationRepository.save(savedCalculationMock)).thenReturn(savedCalculationMock);
		
		// when
		var realCalculation = calculationService.digitoUnico(calculationDTO);
		
		// Assert
		assertEquals(8, realCalculation.getResult().intValue());
		assertNull(realCalculation.getUserId());
	}

}
